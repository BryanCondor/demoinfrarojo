package com.bryan.alexander.demo;

import android.app.Activity;
import android.content.Context;
import android.hardware.ConsumerIrManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.util.Consumer;

public class GestorInfrarojo {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public GestorInfrarojo(Activity activity) {

        ConsumerIrManager consumerIrManager;
        consumerIrManager = (ConsumerIrManager) activity.getSystemService(Context.CONSUMER_IR_SERVICE);

        int[] pattern = {1901, 4453, 625, 1614, 625, 1588, 625, 1614, 625, 442, 625, 442, 625,
                468, 625, 442, 625, 494, 572, 1614, 625, 1588, 625, 1614, 625, 494, 572, 442, 651,
                442, 625, 442, 625, 442, 625, 1614, 625, 1588, 651, 1588, 625, 442, 625, 494, 598,
                442, 625, 442, 625, 520, 572, 442, 625, 442, 625, 442, 651, 1588, 625, 1614, 625,
                1588, 625, 1614, 625, 1588, 625, 48958};

        consumerIrManager.transmit(38400, pattern);

    }
}
